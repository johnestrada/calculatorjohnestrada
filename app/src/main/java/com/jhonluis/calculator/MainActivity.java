package com.jhonluis.calculator;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.NoSuchElementException;

import static java.lang.Math.sqrt;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btnAdd, btnSub, btnMul, btnDiv, btnClr, btnSqrt, btnPow;
    private TextView tvresult;
    private EditText texta,textb,textc;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        btnAdd =(Button)findViewById(R.id.btn1);
        btnSub =(Button)findViewById(R.id.btn2);
        btnMul =(Button)findViewById(R.id.btn3);
        btnDiv =(Button)findViewById(R.id.btn4);
        btnSqrt =(Button)findViewById(R.id.btn5);
        btnPow =(Button)findViewById(R.id.btn6);
        btnClr =(Button)findViewById(R.id.btn0);

        texta = (EditText)findViewById(R.id.text1);
        textb =(EditText)findViewById(R.id.text2);
        textc =(EditText)findViewById(R.id.text0);
        tvresult = (TextView)findViewById(R.id.result);

        btnAdd.setOnClickListener(this);
        btnSub.setOnClickListener(this);
        btnMul.setOnClickListener(this);
        btnDiv.setOnClickListener(this);
        btnSqrt.setOnClickListener(this);
        btnPow.setOnClickListener(this);
        btnClr.setOnClickListener(this);

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onClick(View view) {
        String a="" ;
        String b="" ;
        double num1 = 0.0 ;
        double num2 = 0.0 ;

        try {
            a = texta.getText().toString();
            b = textb.getText().toString();
            num1 = ConvertInfixToPostfix.result(a);
            num2 = ConvertInfixToPostfix.result(b);
        }catch(Exception e) {
            Log.i("text", "No input");
        }
            Log.i("text", a);
            Log.i("text", b);
            Log.i("eval", String.valueOf(num1));
            Log.i("eval", String.valueOf(num2));
            switch (view.getId()) {
                case R.id.btn1:
                    double addition = num1 + num2;
                    textc.setText(String.valueOf(addition));
                    break;
                case R.id.btn2:
                    double subtraction = num1 - num2;

                    textc.setText(String.valueOf(subtraction));
                    break;
                case R.id.btn3:
                    double multiplication = num1 * num2;
                    textc.setText(String.valueOf(multiplication));
                    break;
                case R.id.btn4:
                    double division = num1 / num2;
                    textc.setText(String.valueOf(division));
                    break;
                case R.id.btn5:
                    double power = Math.sqrt(num1);
                    textc.setText(String.valueOf(power));
                    break;
                case R.id.btn6:

                    double square = Math.pow(num1, num2);
                    textc.setText(String.valueOf(square));
                    break;
                case R.id.btn0:
                    texta.getText().clear();
                    textb.getText().clear();
                    textc.getText().clear();
                    break;
            }
        }



        }

