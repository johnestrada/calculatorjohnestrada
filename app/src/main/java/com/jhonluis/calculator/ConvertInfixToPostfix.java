package com.jhonluis.calculator;

/**
 * Created by jhonl on 11/26/17.
 */

import java.util.Stack;
import java.util.Scanner;
import java.util.regex.Pattern;

public class ConvertInfixToPostfix
{

    //main
    public static double result(String expression)
    {
        String answer;
        double number=0;
        Scanner stdin = new Scanner(expression);
        do
        {
            System.out.print("Your expression: ");
            expression = stdin.nextLine( );
            if(isBalanced(expression)==true)
            {
                try
                {
                    answer = infix2postfix(expression);
                    double eval=evaluate(answer);
                    number= eval;
                }
                catch (Exception e)
                {
                    System.out.println("Error." + e.toString( ));
                }
            } else System.out.println("expression not balanced");
        } while (false);

        return number;
    }


    public static boolean isBalanced (String infixEx)
    {
        Stack <Character> store = new Stack <Character>();
        boolean failed =false;
        for (int i=0;(i<infixEx.length());i++)
        {
            if (infixEx.charAt(i)=='(')
                store.push(infixEx.charAt(i));
            if (infixEx.charAt(i)==')')
            {
                if(store.isEmpty()||(store.pop()!='('))
                    failed=true;
            }

        }
        return (store.isEmpty()&&!failed);
    }

    //************************//
    public static boolean query(Scanner input, String prompt)
    {
        String answer;

        System.out.print(prompt + " [Y or N]: ");
        answer = input.nextLine( ).toUpperCase( );
        while (!answer.startsWith("Y") && !answer.startsWith("N"))
        {
            System.out.print("Invalid response. Please type Y or N: ");
            answer = input.nextLine( ).toUpperCase( );
        }

        return answer.startsWith("Y");
    }

    /**
     * return the postfix expression from an infix expression
     * @param s a String
     */
    public static String infix2postfix(String s)
    {
        Scanner input = new Scanner(s);
        Stack<String> operands = new Stack<String>( );
        Stack<Character> operations = new Stack<Character>( );
        String next;
        char first;

        while (input.hasNext( ))
        {
            if (input.hasNext(UNSIGNED_DOUBLE))
            {
                next = input.findInLine(UNSIGNED_DOUBLE);
                operands.push(next);
            }
            else
            {
                next = input.findInLine(CHARACTER);
                first = next.charAt(0);

                switch (first)
                {
                    case '+': // Addition
                    case '-': // Subtraction
                    case '*': // Multiplication
                    case '/': // Division
                        operations.push(first);
                        break;
                    case ')': // Right parenthesis
                        evaluateStackTops(operands, operations);
                        break;
                    case '(': // Left parenthesis
                        break;
                    default : // Illegal character
                        throw new IllegalArgumentException("Illegal not parenthesized Expression");
                }
            }
        }
        if (operands.size( ) != 1)
            throw new IllegalArgumentException("Expression not fully parenthesized");
        return operands.pop( );
    }

    /**
     * return the result of the postfix expression
     * @param postfix a String
     */
    public static double evaluate(String postfix){
        Stack <Double> operands = new Stack<Double>();

        Scanner  scanPostfix = new Scanner(postfix);
        double result=0;
        while(scanPostfix.hasNext()){
            if(scanPostfix.hasNextDouble()){

                operands.push(scanPostfix.nextDouble());
                continue; }
            double first=operands.pop();
            double second=operands.pop();
            char operator = scanPostfix.next().charAt(0);

            switch (operator)
            {
                case '+': // Addition
                    operands.push(second+first);
                    continue;
                case '-': // Subtraction
                    operands.push(second-first);
                    continue;
                case '*': // Multiplication
                    operands.push(second*first);
                    continue;
                case '/': // Division
                    operands.push(second/first);
                    continue;
            }
        }
        result=operands.pop();
        return result;
    }

    //************************//
    public static void evaluateStackTops(Stack<String> operands,
                                         Stack<Character> operations)

    {
        String operand1, operand2;

        if ((operands.size( ) < 2) || (operations.isEmpty( )))
            throw new IllegalArgumentException("Illegal expression");
        operand2 = operands.pop( );
        operand1 = operands.pop( );

        switch (operations.pop( ))
        {
            case '+': operands.push(operand1 + " " + operand2 + " + ");
                break;
            case '-': operands.push(operand1 + " " + operand2 + " - ");
                break;
            case '*': operands.push(operand1 + " " + operand2 + " * ");
                break;
            case '/': operands.push(operand1 + " " + operand2 + " / ");
                break;
            default : throw new IllegalArgumentException("Illegal operation");
        }
    }

    // These patterns are from Appendix B of Data Structures and Other Objects.
    // They may be used in hasNext and findInLine to read certain patterns
    // from a Scanner.
    public static final Pattern CHARACTER =
            Pattern.compile("\\S.*?");
    public static final Pattern UNSIGNED_DOUBLE =
            Pattern.compile("((\\d+\\.?\\d*)|(\\.\\d+))([Ee][-+]?\\d+)?.*?");




}
